const mongoose = require('mongoose')

module.exports = async (context) => {
  mongoose.Promise = global.Promise

  const {schemas, database} = context
  const {name, host, port} = database
  const uri = `mongodb://${host}:${port}/${name}`
  await mongoose.connect(uri, {useMongoClient: true})

  const models = Object.keys(schemas || {}).reduce((models, schemaRef) => {
    const indexOfName = schemaRef.indexOf('#') + 1
    let schemName = schemaRef.substring(indexOfName, schemaRef.length)
    if (schemName.substr(-6) === 'Schema') schemName = schemName.substring(0, schemName.length - 6)
    models[schemName] = mongoose.model(schemName, schemas[schemaRef])
    return models
  }, {})

  Object.assign(context, {models, mongoose})
}
